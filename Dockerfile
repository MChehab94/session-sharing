FROM node:4.4.7
RUN echo starting

# ADDING NON-ROOT USER
RUN groupadd -r app && useradd -r -g app app

# INSERTING CODE
RUN mkdir /src
ADD . /src
WORKDIR /src
RUN npm install && chown app:app /src -R

# SETTING UP ENVIORNMENT
ENV RUNNING_IN_CONTAINER true
ENV PORT 4000
EXPOSE 4000

CMD ["/usr/local/bin/node","/src/index.js"]