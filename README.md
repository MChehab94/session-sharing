# Authenticated Session Injection

The goal of this challenge is to authenticate a user into accounts without a username and password for every account. By injecting an authenticated session into the browser, the client is able to authenticate to the server, bypassing the username password step.

In this challenge the user is prompted to enter a single sign-on password (master password) that allows him to access his kickstarter account.



The benefits of this method are numerous, to list a few:

[x] share access to an account without sharing the password
[x] revoke access to an account remotely by invalidating the session 


## Technical Implementation

All the required information to implement the challenge from a technical aspect are covered in the following section. **Don't hesitate to reach out for any clarification or if you suspect that the directions are inaccurate**, after all, teamwork is at the essence of everything that we do as engineers.

### Stage 1: Getting the Cookies

- Request Password from user
- Base64 the password
- Emit event containing encoded password to get a session:
	**event name**: requestSession
	**encoded password label**: token

- Listen to event containing session:
	**event name**: requestSession
	**session label**: sessionToken

- Place session received in headers of socket (might need to reconnect after that)

- Emit event to get kickstarter authenticated cookies:
	**event name**: requestLogin
- Listen to event containing kickstarter authenticated cookies:
	**event name**: requestLogin
	fields:
		**site** (String) #contains site name
		**token** (JSON String) #contains kickstarter cookies


### Stage 2: Injecting the Cookies

- Decode the stringified JSON cookies
- Inject the cookies in a webview
- Reload the kickstarter page, you should be logged in!

`hint: you might have to modify/add/remove some fields in the cookies for them to be compatible.`

### Required Information

**Socket library: socket.io 

**Socket server address:** 52.26.56.105

**Socket server port:** 4000

**Password:** mykimykimykitoken


## Design Guidelines

Design is at the essence of myki. We strongly believe that our code is only as good as its representation to our users.

For this challenge we will stick with our basic theme.

**Page Background**: `#191919`

**Bottom Bar Background**: `#323232`

**Bottom Bar Shadow**: `color rgba(0,0,0,0.5) distance 2px direction top; spread 4px`

**myki green**: `#2dbf90` (memorize that)

**text color**: `#fff`

### Screenshot

![](screenshots/verificationCode.png) Password Input Page

The webview should then open as a modal and fill the entire screen.

